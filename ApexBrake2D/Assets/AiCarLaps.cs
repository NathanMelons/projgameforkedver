﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiCarLaps : MonoBehaviour {

	public int AilapCount;

	// Use this for initialization
	void Start () {
		
	}

	void OnTriggerEnter2D(Collider2D finishLine)
	{
		if (finishLine.gameObject.name == "startFinish") {//finds game object with this specific name and adds value to counter
			AilapCount++;
			Debug.Log ("Lap: " + AilapCount);
		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
