﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour {

	public Transform followTarget;//transform will contain the object for the camera to follow

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(followTarget.position.x, followTarget.position.y, -10f);//camera position will stay on specified target 
	}
}
