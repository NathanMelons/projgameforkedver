﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserCarController : MonoBehaviour {

	float carSpeed=12f;
	float carTorque=-160f;
	float carDrift=0.9f;
	public int lapCounter;
	public int carHealth=25;
	//public Text Laps;
	//public int AilapCount;

	// Use this for initialization
	void Start () {
		
	}

	void OnTriggerEnter2D(Collider2D finishLine)
	{
		if (finishLine.gameObject.name == "startFinish") {//finds game object with this specific name and adds value to counter
			lapCounter++;
			//Laps.text=lapCounter.ToString ();
			Debug.Log ("Lap: " + lapCounter);
			if (lapCounter == 4) {
				SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
				//gets active scene index number and adds 1 to said index therefore loading the next scene with the corresponding index

				//UnityEditor.EditorApplication.isPlaying = false;
			}
			//if (AilapCount == 4) { was to be used for AI so it will have its own lap system like user car
			//	SceneManager.LoadScene ("loserscreen");
			//}
				
		}

	}

	void OnCollisionEnter2D(Collision2D wall)
	{
		if (wall.gameObject.name == "walls") {
			carHealth--;
			Debug.Log ("Car health: " + carHealth);
			if (carHealth == 0) {
				SceneManager.LoadScene ("loserscreen");
			}
		}
	}


	// Update is called once per frame
	void FixedUpdate () {//fixed runs once per tick of the physics engine
		Rigidbody2D carrb=GetComponent<Rigidbody2D>();
		carrb.velocity = carForwardSpeed()+carRightSpeed()*carDrift;//sets velocity from the previous frame to reduce the sideways velocity
		//setting velocity to only be the forward speed

		if (Input.GetButton ("Accelerate")) {//this makes use of an element with a name from the Inputs window rather than hard coding specific keys
			//returns true in every frame that the button is being held down
			carrb.AddForce(transform.up*carSpeed);//multiply the up vector with the car's speed
		}
		carrb.angularVelocity= Input.GetAxis("Horizontal")*carTorque;//setting angular velocity to be equal to the sideways velocity
		//car veolcity is forced to become its forward speed
	}
	Vector2 carForwardSpeed(){//calculates how much speed the car is generating going forwards
		return transform.up* Vector2.Dot(GetComponent<Rigidbody2D>().velocity,transform.up);//dot sees the total velocity and tells how much of it is going in a forwards direction
				
	}
	Vector2 carRightSpeed(){//calculates how much speed the car is generating going to the right
		return transform.right * Vector2.Dot (GetComponent<Rigidbody2D> ().velocity, transform.right);
	}
}
