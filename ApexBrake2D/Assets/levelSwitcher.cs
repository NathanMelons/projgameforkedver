﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class levelSwitcher : MonoBehaviour {

	//public AudioClip WildRace;

	// Use this for initialization
	public void loadLevel(string levelName)
	{
		SceneManager.LoadScene (levelName);
	}
		

	void Start () {
		
	}
	public void load1stLvl()
	{
		SceneManager.LoadScene ("level1");
		//GetComponent<AudioSource> ().clip = WildRace;
		//GetComponent<AudioSource> ().Play;

	}

	public void bkToMenu()
	{
		SceneManager.LoadScene ("mainMenu");
	}
		
	public void Quit(){
		//UnityEditor.EditorApplication.isPlaying = false;
		Application.Quit ();
	}

	public void loadNextLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

}
