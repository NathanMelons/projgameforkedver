﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicplayer : MonoBehaviour {

	static musicplayer racemusic = null;
	// Use this for initialization
	void Awake()//first method called by unity so the gameobject is being deleted when the app starts running
	{
		if (racemusic != null) {
			Debug.Log ("removing" + this.gameObject.GetInstanceID ().ToString ());//destroys new musicplayer to prevent multiple music replays
			Destroy (this.gameObject);
		} else
		{
			racemusic = this;
			GameObject.DontDestroyOnLoad (this.gameObject);//object is not destroyed when new scene is loaded so music will keep playing
		}
	}


	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
